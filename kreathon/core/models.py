from django.db import models


class User(models.Model):
    """
    Django model representing a user.

    Attributes:
        id (AutoField): The primary key for the user.
        email (EmailField): The email address of the user.

    Methods:
        __str__: Returns a string representation of the user, using the email address.
    """

    id = models.AutoField(primary_key=True)
    email = models.EmailField(max_length=200)

    def __str__(self):
        """
        String representation of the user.

        Returns:
            str: The email address of the user.
        """
        return self.email


class Footprint(models.Model):
    """
    Django model representing a footprint.

    Attributes:
        id (AutoField): The primary key for the footprint.
        start_time (CharField): The start time of the footprint.
        end_time (CharField): The end time of the footprint.
        distance (FloatField): The distance covered in the footprint.
        transport (CharField): The mode of transportation for the footprint.
        co2 (FloatField): The carbon dioxide emissions of the footprint.
        fuel_price (FloatField): The fuel price associated with the footprint.
        user (ForeignKey): The foreign key linking the footprint to a user.

    Methods:
        __str__: Returns a string representation of the footprint.

    Foreign Keys:
        user (User): The user associated with the footprint.
    """

    id = models.AutoField(primary_key=True)
    start_time = models.CharField(max_length=200)
    end_time = models.CharField(max_length=200)
    distance = models.FloatField()
    transport = models.CharField(max_length=200)
    co2 = models.FloatField()
    fuel_price = models.FloatField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        """
        String representation of the footprint.

        Returns:
            str: A string representation of the footprint, including the associated user's email.
        """
        return f"{self.user.email}'s Footprint {self.id}"
