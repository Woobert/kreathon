"""
Django view function for handling the index page.

This view function is configured to handle both GET and POST requests.
- GET requests render the "core/index.html" template.
- POST requests expect JSON data containing latitude, longitude, and acceleration values.
  The function extracts this data, prints it, and returns a JSON response with the received data.

Parameters:
    request (HttpRequest): The HTTP request object.

Returns:
    JsonResponse: A JSON response containing either the extracted data or an error message.

Exceptions:
    JSONDecodeError: Raised if the incoming JSON data is not valid.

Note:
    This view is decorated with @csrf_exempt to disable CSRF protection for simplicity.
    In production, consider enabling CSRF protection and ensuring the proper use of CSRF tokens.
"""
import json
import datetime
import sqlite3
from geopy import distance
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from core.models import User, Footprint

def save_data():
    # initialise user name
    user = "user"
    # def get_user_data():

    # placeholder: here we would collect user data (gps, movement, ...) and do various calculations to determine:
    # a) gps coordinates b) transport mode c) timestamp

    # dummy data for testing purpose
    # GPS coordinates
    lat = 51.325950
    lon = 6.570962

    # transport mode
    transport_mode = "local_train"

    # taken timestamp
    timestamp = 1671234567

    ### BEGIN ###
    while True:
        # for every new transport mode the distance is declared as 0
        distance_km = 0

        # build json structure
        json_obj = {
            "distance": distance_km,
            "gps": [lon, lat],
            "transport_mode": transport_mode,
            "time": timestamp
        }

        # append data to temporary storage (json)
        with open("temp.json", "w") as f:
            json.dump(json_obj, f, indent=4)

        # placeholder: collect user data get_user_data(...)

        # NEW GPS coordinates
        new_lat = 51.327083
        new_lon = 6.568705

        # NEW transport mode
        new_transport_mode = "train"

        # NEW taken timestamp
        new_timestamp = 1671234567

        # add distance between both gps coordinates in km
        with open("temp.json", "w") as f:
            json_obj["distance"] = json_obj["distance"] + distance.distance((lat, lon), (new_lat, new_lon)).km
            json.dump(json_obj, f)

        # read newest version of json
        with open("temp.json", "r") as f:
            json_obj = json.load(f)

        timestamp = json_obj["time"]
        transport_mode = json_obj["transport_mode"]
        spec_distance = json_obj["distance"]

        # check if mode of transport changed, would actually be something like:
        # if get_userdata(...)[1] != transport_mode:

        if new_transport_mode != transport_mode:
            # TODO collect average co2 consumption
            #with open("co2.json", "r") as f:
            #    # get specific consumption
            #    consumption_per_km = json.load(f)[transport_mode]

            consumption_per_km = 30

            # calculate consumption
            consumption = consumption_per_km * spec_distance

            # insert data into sqlite database

            # get user id
            spec_user = User.objects.filter(email=user)[0]

            new_footprint = Footprint(start_time=timestamp, end_time=new_timestamp, distance=distance_km, transport=transport_mode, co2=consumption, fuel_price=0, user=spec_user)
            new_footprint.save()

            # overwrite temporary storage (json) => next iteration
@csrf_exempt
def index(request):
    now = datetime.datetime.now()

    if request.method == "POST":
        try:
            data = json.loads(request.body)
            latitude = data.get("latitude")
            longitude = data.get("longitude")
            x_acceleration = data.get("x_acceleration")
            y_acceleration = data.get("y_acceleration")
            z_acceleration = data.get("z_acceleration")

            response_data = {
                "latitude": latitude,
                "longitude": longitude,
                "x_acceleration": x_acceleration,
                "y_acceleration": y_acceleration,
                "z_acceleration": z_acceleration,
            }

            print(response_data)
            return JsonResponse(response_data)

        except json.JSONDecodeError as e:
            return JsonResponse({"error": "Invalid JSON format"}, status=400)

    return render(request, "core/index.html", {})

# link Mapping, with parameters
# @csrf_exempt
# def maps(request):

# link cakechart with paramters
# @csrf_exempt
# def cake(request):