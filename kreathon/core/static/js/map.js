document.addEventListener('DOMContentLoaded', function() {
    var map = L.map('mapid').setView([51.325950, 6.570962], 13);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '© OpenStreetMap contributors'
    }).addTo(map);

    var marker = L.marker([51.325950, 6.570962]).addTo(map);
    marker.bindPopup("<b>Ich</b><br>bin hier").openPopup();
});
