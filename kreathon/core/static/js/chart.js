document.addEventListener('DOMContentLoaded', function() {
    // Statisch definierte Daten
    var dataLabels = ["Auto", "Bahn", "Bus", "zu Fuß", "Fahrrad"];
    var dataValues = [12, 19, 3, 27, 10];

    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: dataLabels,
            datasets: [{
                data: dataValues,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(75, 192, 192, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(75, 192, 192, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            responsive: true, // Macht das Diagramm responsiv
            maintainAspectRatio: false,
            plugins: {
                legend: {
                    labels: {
                        color: 'white' // Setzt die Farbe der Legendenbeschriftung auf Weiß
                    }
                }
            }
        }
    });
});