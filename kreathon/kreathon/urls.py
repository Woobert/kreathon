from django.contrib import admin
from django.urls import path
from core.views import index


urlpatterns = [path("admin/", admin.site.urls), path("", index, name="index")]

"""
Django URL configuration for the admin panel.

This path maps to the Django admin site.

Example:
    To access the admin panel, navigate to "/admin/" in the URL.

Usage:
    path("admin/", admin.site.urls),


Django URL configuration for the root path.

This path maps to the 'index' view in the 'core' app.

Example:
    To access the 'index' view, navigate to the root URL ("/") in the browser.

Usage:
    path("", index, name="index"),
"""
