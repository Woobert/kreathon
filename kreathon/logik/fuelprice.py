import requests


def get_fuel_price(lat, lng, fuel_type):
    """
    Retrieves the fuel price for a specific fuel type at the nearest gas station based on given coordinates.

    Args:
        lat (float): The latitude of the location.
        lng (float): The longitude of the location.
        fuel_type (str): The type of fuel to retrieve prices for (e.g., 'diesel', 'e5', 'e10').

    Returns:
        float or str: The fuel price in euros per liter if successful. If there's an error, returns an error message.

    Note:
        The function makes a request to the Tankerkönig API to obtain fuel prices for nearby gas stations.
        It uses the provided latitude, longitude, and fuel type as parameters for the API request.

    Example:
        >>> lat = 52.521
        >>> lng = 13.438
        >>> fuel_type = "diesel"  # Choose from 'diesel', 'e5', 'e10'
        >>> print(get_fuel_price(lat, lng, fuel_type))
        1.729  # Output is the diesel fuel price at the nearest gas station in euros per liter.
    """
    api_url = "https://creativecommons.tankerkoenig.de/json/list.php"
    parameters = {
        "lat": lat,
        "lng": lng,
        "rad": 1.5,
        "sort": "dist",
        "type": "all",
        "apikey": "2405be53-6ade-72d1-3f89-41c73a749872",
    }

    response = requests.get(api_url, params=parameters)

    if response.status_code == 200:
        data = response.json()
        first_station = data["stations"][0]
        return first_station.get(fuel_type)
    else:
        return f"Error: {response.status_code}"


lat = 52.521
lng = 13.438
fuel_type = "diesel"

print(get_fuel_price(lat, lng, fuel_type))
