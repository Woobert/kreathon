import requests


def get_vehicle(gps: tuple, gyro: tuple) -> str:
    """
    Determines the mode of transportation based on GPS and gyroscope data.

    Args:
        gps (tuple): A tuple representing the GPS coordinates (latitude, longitude).
        gyro (tuple): A tuple representing the gyroscope data (x, y, z).

    Returns:
        str: The identified mode of transportation ('foot', 'bike', 'car', 'train').

    Notes:
        - If the user is moving, it checks for walking/cycling or car/train based on the gyroscope data.
        - If the user is not moving, it checks for car/train based on the road type obtained from the Overpass API.
    """

    def get_acc(gyro: tuple) -> float:
        """
        Calculates the maximum acceleration from gyroscope data.

        Args:
            gyro (tuple): A tuple representing the gyroscope data (x, y, z).

        Returns:
            float: The maximum acceleration value.
        """
        return max(gyro)

    def is_moving(gyro: tuple) -> bool:
        """
        Checks if the user is moving based on gyroscope data.

        Args:
            gyro (tuple): A tuple representing the gyroscope data (x, y, z).

        Returns:
            bool: True if the user is moving, False otherwise.
        """
        return get_acc(gyro) > 3

    def get_walk_or_cycle(gyro: tuple) -> str:
        """
        Determines whether the user is walking or cycling based on gyroscope data.

        Args:
            gyro (tuple): A tuple representing the gyroscope data (x, y, z).

        Returns:
            str: 'foot' if walking, 'bike' if cycling.
        """
        if get_acc(gyro) > (20 / 3.6):
            return "bike"
        return "foot"

    def get_car_or_train(gps: tuple) -> str:
        """
        Determines whether the user is in a car or a train based on road type obtained from the Overpass API.

        Args:
            gps (tuple): A tuple representing the GPS coordinates (latitude, longitude).

        Returns:
            str: 'car' if on a road, 'train' if on a railway, 'foot' otherwise.
        """
        road = get_road_type(gps)
        if road == "rail":
            return "train"
        elif road == "road":
            return "car"
        return "foot"

    def get_road_type(gps: tuple, radius: int = 10) -> str:
        """
        Gets the type of road (rail, road, other) based on Overpass API and GPS coordinates.

        Args:
            gps (tuple): A tuple representing the GPS coordinates (latitude, longitude).
            radius (int): The radius in meters for Overpass API query.

        Returns:
            str: The type of road ('rail', 'road', 'other').

        Raises:
            Exception: If there is an error with the Overpass API request.
        """

    def get_road_type(gps, radius=10):
        lat = gps[0]
        lon = gps[1]
        overpass_url = "http://overpass-api.de/api/interpreter"

        # Overpass query to check for highways within a specified radius
        highway_query = f"""
        [out:json];
        way
        (around:{radius},{lat},{lon})
        ["highway"];
        (._;>;);
        out body;
        """

        # Overpass query to check for railways within a specified radius
        railway_query = f"""
        [out:json];
        way
        (around:{radius},{lat},{lon})
        ["railway"];
        (._;>;);
        out body;
        """

        params_highway = {"data": highway_query}

        params_railway = {"data": railway_query}
        try:
            response_highway = requests.get(overpass_url, params=params_highway)
            response_railway = requests.get(overpass_url, params=params_railway)

            data_highway = response_highway.json()
            data_railway = response_railway.json()

            if len(data_railway["elements"]) > 0:
                return "rail"
            elif len(data_highway["elements"]) > 0:
                print(data_highway["elements"])
                return "road"
            return "other"

        except requests.exceptions.RequestException as e:
            print(f"Overpass API Error: {e}")

    if is_moving(gyro):
        return get_walk_or_cycle(gyro)
    return get_car_or_train(gps)


testtrain = (52.2702, 13.4366)
testcar = (52.2665, 13.4589)
testfoot = (52.2923, 13.4840)
stand = (0, 0, 0)
walk = (4, 4, 4)
bike = (6, 6, 6)
print(get_vehicle(testtrain, stand))
