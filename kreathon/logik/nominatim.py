import requests


def get_street(lat, lng):
    """
    Retrieves information about the street at the given coordinates using the Nominatim OpenStreetMap API.

    Args:
        lat (float): The latitude of the location.
        lng (float): The longitude of the location.

    Returns:
        dict: A dictionary containing information about the street.
            - "category" (str): The category of the street (e.g., 'highway', 'residential', etc.).
            - "street_type" (str): The type of the street (e.g., 'primary', 'secondary', etc.).

    Note:
        The function makes a reverse geocoding request to the Nominatim OpenStreetMap API to obtain information
        about the street at the specified latitude and longitude.

    Example:
        >>> lat = 51.312211
        >>> lng = 6.557178
        >>> print(get_street(lat, lng))
        {'category': 'highway', 'street_type': 'unclassified'}
    """
    response = requests.get(
        f"https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat={lat}&lon={lng}"
    )

    res = {
        "category": response.json()["category"],
        "street_type": response.json()["type"],
    }

    return res


lat = 51.312211
lng = 6.557178

print(get_street(lat, lng))
