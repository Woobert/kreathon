# Documentation for `index.html`

## Description
The `index.html` file contains an HTML document that includes JavaScript code. It serves as the frontend for a web page that asynchronously retrieves accelerometer data, geolocation information, and sends a POST request to a Django server. The django sever calculated the transportation medium and saves the users paths into the database. 
To launch the application, ensure that Django is installed, and add the necessary hosts to the settings.py file. Once configured, initiate the application by running the command python manage.py runserver. Now the user can acceses to Webserver at the configured host domain.



## HTML Structure
- `<!DOCTYPE html>`: Document type declaration.
- `<html lang="en">`: HTML root element with language attribute.
- `<head>`: Contains meta tags, title, and a script tag for jQuery.
- `<body>`: The main content of the HTML document.

## JavaScript Functions

### `getAcceleration()`
- Asynchronous function to retrieve accelerometer data.
- Throws an error if the accelerometer is not supported.
- Returns a promise with an array of acceleration values `[x, y, z]`.

### `getLocation()`
- Asynchronous function to retrieve the user's geolocation.
- Throws an error if geolocation permission is denied or an error occurs during retrieval.
- Returns a promise with an array of latitude and longitude `[latitude, longitude]`.

### Document-Ready Block
- jQuery document-ready block to handle asynchronous operations on page load.
- Calls `getLocation()` and `getAcceleration()` functions.
- Sends a POST request to a Django server with retrieved data.
- Updates the content of an HTML element with the received data or error message.

## Usage
1. Include the necessary meta tags and scripts in the head.
2. Place the accelerometer and geolocation functions in the script tag.
3. Use the jQuery document-ready block to initiate asynchronous operations.

# Documentation for `views.py`

## Description
The `views.py` file contains a Django view function for handling the index page. It is configured to handle both GET and POST requests.

## View Function: `index(request)`
- Handles GET requests by rendering the "core/index.html" template.
- Handles POST requests by extracting JSON data (latitude, longitude, acceleration) from the request.
- Prints the extracted data and returns a JSON response with the received data or an error message.

## Parameters
- `request` (HttpRequest): The HTTP request object.

## Returns
- `JsonResponse`: A JSON response containing either the extracted data or an error message.

## Exceptions
- `JSONDecodeError`: Raised if the incoming JSON data is not valid.

## Note
- The view is decorated with `@csrf_exempt` to disable CSRF protection for simplicity.
- In production, consider enabling CSRF protection and ensuring proper use of CSRF tokens.

# Documentation for `fuelprice.py`

## Description
The `fuelprice.py` file contains a function to retrieve fuel prices for a specific fuel type at the nearest gas station based on given coordinates.

## Function: `get_fuel_price(lat, lng, fuel_type)`
- Retrieves fuel price for a specific fuel type at the nearest gas station.
- Makes a request to the Tankerkönig API using latitude, longitude, and fuel type.
- Returns fuel price in euros per liter if successful, or an error message.

## Args
- `lat` (float): Latitude of the location.
- `lng` (float): Longitude of the location.
- `fuel_type` (str): Type of fuel to retrieve prices for (e.g., 'diesel', 'e5', 'e10').

## Returns
- `float` or `str`: Fuel price if successful, or an error message.

## Example
```python
lat = 52.521
lng = 13.438
fuel_type = "diesel"
print(get_fuel_price(lat, lng, fuel_type))

# Output: 1.729 (diesel fuel price at the nearest gas station in euros per liter)
```

# Documentation for `nominatim.py`

## Description
The `nominatim.py` file contains a function to retrieve information about the street at given coordinates using the Nominatim OpenStreetMap API.

## Function: `get_street(lat, lng)`
- Retrieves information about the street at given coordinates.
- Makes a reverse geocoding request to the Nominatim OpenStreetMap API.
- Returns a dictionary with street information.

## Args
- `lat` (float): Latitude of the location.
- `lng` (float): Longitude of the location.

## Returns
- `dict`: Street information (category and street type).

## Example
```python
lat = 51.312211
lng = 6.557178
print(get_street(lat, lng))
# Output: {'category': 'highway', 'street_type': 'unclassified'}
```

# Documentation for `vehicle.py`

## Description
The `vehicle.py` file contains a function to determine the mode of transportation based on GPS and gyroscope data.

## Function: `get_vehicle(gps, gyro)`
- Determines transportation mode based on GPS and gyroscope data.
- Checks for walking/cycling or car/train based on gyroscope data if moving.
- Checks for car/train based on road type obtained from the Overpass API if not moving.

## Args
- `gps` (tuple): GPS coordinates (latitude, longitude).
- `gyro` (tuple): Gyroscope data (x, y, z).

## Returns
- `str`: Identified mode of transportation ('foot', 'bike', 'car', 'train').

## Notes
- If the user is moving, it checks for walking/cycling or car/train based on gyroscope data.
- If the user is not moving, it checks for car/train based on the road type obtained from the Overpass API.

## Example
```python
testtrain = (52.2702, 13.4366)
testcar = (52.2665, 13.4589)
testfoot = (52.2923, 13.4840)
stand = (0, 0, 0)
walk = (4, 4, 4)
bike = (6, 6, 6)
print(get_vehicle(testtrain, stand))
# Output: 'train'

